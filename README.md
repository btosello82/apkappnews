# ApkAppNews

#### ANP News (ANP News.com.ibm.anpnews)

- **v1.2.6**:
    >- Se corrigió un error que hacía que al borrar la lista de mensajes desde el tag de Pushes, limpiaba el inicio de sesión al reiniciar la aplicación.
    >- Se agregó un Toast (mensaje en borde inferior) al abrir la aplicación que muestra el nombre de usuario de la sesión que se está iniciando.

- **v1.2.5**:
    >- Se agregó la suscripción al canal "Mail Change".

- **v1.2.4**:
    >- Se modificaron métodos para evitar que se muestren mensajes duplicados en la lista de Pushes.

- **v1.2.3**:
    >- Se modificó el tipo de letra cuando se recibe un Push con sólamente Content (sin Heading).
    >- Se corrigió el ordenamiento de los Pushes en la lista, cronológicamente (tenía formato de fecha 'dd/mm/yyyy', cuando esperaba un formato 'mm/dd/yyyy').
    >- Se está modificando la visibilidad de los tableros en el tab DataDog (in progress).

- **v1.2.2**:
    >- Se corrigió la cabecera de la aplicación:
    >    - Se aumentó el tamaño del logo de la empresa.
    >    - Se corrigió los tamaños de los títulos.
    >    - Se acomodó para que sea 100% responsive.
    >- Se modificaron los endpoints de las peticiones HTTP, ahora apunta a HOMO de IBM Cloud Naranja.

- **v1.2.1**:
    >- Se corrigió un error que permitía a todos los usuarios manipular el envío de mensajes desde la App.
    >- Se agregó a Benjamín Tosello para el envío de notificaciones Push desde la App.
    >- Se modificó el protocolo HTTP de los Endpoints de la API, para permitir su acceso a todos los celulares. Modificada a HTTPS.
    >- Se solucionó un error que impedía recibir las notificaciones rechazadas desde la Barra de Estado de Android, y cuando la App estaba cerrada. (Problema generado por la incorporación del sistema de Login en release anterior).

- **v1.2.0**:
    >- Minor:
    >    - Se agregó un Sistema de login simple, el Register incluye Nombre y Email, mientras que el Login requiere el Nombre para iniciar el >sistema.
    >- Patch:
    >    - Pantallas de Login/Register al iniciar la App.
    >    - Validaciones para Login y Register.
    >    - Sólo se admite 1 (UN) registro por dispositivo.
    >    - Una vez iniciada la sesión, se guarda en el Storage del dispositivo, para su rápido acceso a futuro.
    >    - Se agregó un botón de Log Out en el Tab "About", al final de la página (requiere logearse nuevamente para volver a entrar al sistema).
    >    - Se ordenó la lista de mensajes cronológicamente.

- **v1.1.3**:
    >- Se arregló error que impedía la recepción de los mensajes cuando la aplicación estaba cerrada.